#include "midi.h"
#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

struct step {
	uint8_t input;
	midi_message msg;
};

#define feed(b)                                                                \
	{ .input = b }
#define feed_and_yield2(b, st, data0, data1)                                   \
	{                                                                      \
		.input = b, .msg = {.status = st, .data = {data0, data1} }     \
	}

#define feed_and_yield0(b, status) feed_and_yield2(b, status, 0, 0)
#define feed_and_yield1(b, status, data0) feed_and_yield2(b, status, data0, 0)

void run_steps(char *description, struct step steps[], int n) {
	midi_parser parser = {0};
	midi_message msg;
	struct step step;
	int i;

	printf("TEST: %s\n", description);

	for (i = 0; i < n; i++) {
		step = steps[i];
		msg = midi_read(&parser, step.input);

		if (step.msg.status != msg.status ||
		    step.msg.data[0] != msg.data[0] ||
		    step.msg.data[1] != msg.data[1])
			break;
	}

	if (i == n)
		return;

	printf("test failure at step %d (input: 0x%02x)\n", i, step.input);
	printf("expected status=0x%02x data[0]=0x%02x data[1]=0x%02x\n",
	       step.msg.status, step.msg.data[0], step.msg.data[1]);
	printf("actual   status=0x%02x data[0]=0x%02x data[1]=0x%02x\n",
	       msg.status, msg.data[0], msg.data[1]);

	exit(1);
}

void test_note_on(void) {
	struct step steps[] = {
	    /* fresh note on */
	    feed(0x90),
	    feed(0x60),
	    feed_and_yield2(0x64, 0x90, 0x60, 0x64),

	    /* running status */
	    feed(0x10),
	    feed_and_yield2(0x70, 0x90, 0x10, 0x70),

	    /* real time garbage */
	    feed_and_yield0(0xf8, 0xf8),
	    feed(0x11),
	    feed_and_yield0(0xf8, 0xf8),
	    feed_and_yield2(0x25, 0x90, 0x11, 0x25),

	    /* velocity 0 */
	    feed(0x17),
	    feed_and_yield2(0x0, 0x90, 0x17, 0x0),

	    /* new status byte */
	    feed(0x90),
	    feed(0x60),
	    feed_and_yield2(0x64, 0x90, 0x60, 0x64),

	    /* break running status */
	    feed(0xa0),

	    /* unexpected status byte */
	    feed(0x90),
	    feed(0x60),
	    feed(0x90),
	    feed(0x64),
	    feed_and_yield2(0x70, 0x90, 0x64, 0x70),
	};

	run_steps("note on", steps, ARRAY_SIZE(steps));
}

void test_note_on2(void) {
	struct step steps[1 + 2 * 128] = {0};
	int i;

	steps[0].input = 0x90;
	for (i = 1; i < ARRAY_SIZE(steps); i++) {
		struct step *s = steps + i;
		if (i % 2) {
			s->input = (i - 1) / 2;
		} else {
			midi_message *msg = &s->msg;
			s->input = 1;
			msg->status = 0x90;
			msg->data[0] = s[-1].input;
			msg->data[1] = s[0].input;
		}
	}

	run_steps("note on first data byte", steps, ARRAY_SIZE(steps));
}

void test_note_on3(void) {
	struct step steps[3 * 16] = {0};
	int i;

	for (i = 0; i < ARRAY_SIZE(steps); i++) {
		struct step *s = steps + i;
		if (i % 3 == 0) {
			s->input = 0x90 + i / 3;
		} else if (i % 3 == 1) {
			s->input = i;
		} else {
			midi_message *msg = &s->msg;
			s->input = i;
			msg->status = s[-2].input;
			msg->data[0] = s[-1].input;
			msg->data[1] = s[0].input;
		}
	}

	run_steps("note on status byte", steps, ARRAY_SIZE(steps));
}

void test_note_off(void) {
	struct step steps[] = {
	    /* note off */
	    feed(0x80),
	    feed(0x12),
	    feed_and_yield2(0x34, 0x80, 0x12, 0x34),
	    /* running status */
	    feed(0x56),
	    feed_and_yield2(0x78, 0x80, 0x56, 0x78),
	};

	run_steps("note off", steps, ARRAY_SIZE(steps));
}

void test_program_change(void) {
	struct step steps[] = {
	    /* program change */
	    feed(0xc0),
	    feed_and_yield1(0x12, 0xc0, 0x12),
	    /* running status */
	    feed_and_yield1(3, 0xc0, 3),
	};

	run_steps("program change", steps, ARRAY_SIZE(steps));
}

void test_control_change(void) {
	struct step steps[] = {
	    /* control change */
	    feed(0xb0),
	    feed(0x34),
	    feed_and_yield2(0x45, 0xb0, 0x34, 0x45),
	    /* throw in a note on */
	    feed(0x90),
	    feed(0x60),
	    feed_and_yield2(0x64, 0x90, 0x60, 0x64),
	    /* control change */
	    feed(0xb0),
	    feed(0x55),
	    feed_and_yield2(0x67, 0xb0, 0x55, 0x67),
	    /* running status */
	    feed(0x71),
	    feed_and_yield2(0x62, 0xb0, 0x71, 0x62),
	    /* CC 0 */
	    feed(0),
	    feed_and_yield2(0x12, 0xb0, 0x00, 0x12),
	    /* highest channel. just below 0xc0 boundary */
	    feed(0xbf),
	    feed(0x01),
	    feed_and_yield2(0x02, 0xbf, 0x01, 0x02),
	};

	run_steps("control change", steps, ARRAY_SIZE(steps));
}

void test_channel_pressure(void) {
	struct step steps[] = {
	    /* lowest channel pressure status */
	    feed(0xd0),
	    feed_and_yield1(0x01, 0xd0, 0x01),
	    /* running status */
	    feed_and_yield1(0x02, 0xd0, 0x02),
	    /* highest channel pressure status */
	    feed(0xdf),
	    feed_and_yield1(0x03, 0xdf, 0x03),
	};

	run_steps("control change", steps, ARRAY_SIZE(steps));
}

void test_pitch_bend(void) {
	struct step steps[] = {
	    /* pitch bend */
	    feed(0xe0),
	    feed(0x34),
	    feed_and_yield2(0x45, 0xe0, 0x34, 0x45),
	    /* running status */
	    feed(0x56),
	    feed_and_yield2(0x67, 0xe0, 0x56, 0x67),
	};

	run_steps("pitch bend", steps, ARRAY_SIZE(steps));
}

void test_sysex(void) {
	struct step steps[] = {
	    /* start sysex stream */
	    feed(0xf0),
	    feed_and_yield2(1, 0xf0, 1, 0xf0),
	    feed_and_yield1(2, 0xf0, 2),
	    /* real time garbage */
	    feed_and_yield0(0xf8, 0xf8),
	    /* continue sysex stream */
	    feed_and_yield1(3, 0xf0, 3),
	    feed_and_yield0(0xf7, 0xf7),
	    /* trailing sysex data is ignored */
	    feed(0x4),
	};

	run_steps("sysex", steps, ARRAY_SIZE(steps));
}

void test_song_position_pointer(void) {
	struct step steps[] = {
	    feed(0xf2),
	    feed(1),
	    feed_and_yield2(2, 0xf2, 1, 2),
	};

	run_steps("song position pointer", steps, ARRAY_SIZE(steps));
}

void test_single_data_byte(void) {
	int i;
	int single_status[] = {0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6,
			       0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd,
			       0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
			       0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb,
			       0xdc, 0xdd, 0xde, 0xdf, 0xf1, 0xf3};
	struct step steps[ARRAY_SIZE(single_status) * 2 + 2] = {0}, *s;

	for (i = 0, s = steps; i < ARRAY_SIZE(single_status); i++, s += 2) {
		s[0].input = single_status[i];
		s[1].input = i;
		s[1].msg.status = s[0].input;
		s[1].msg.data[0] = s[1].input;
	}

	/* special case: sysex start 0xf0 has second data byte set */
	s[0].input = 0xf0;
	s[1].input = 0x01;
	s[1].msg.status = 0xf0;
	s[1].msg.data[0] = 0x01;
	s[1].msg.data[1] = 0xf0;

	run_steps("statuses with single data byte", steps, ARRAY_SIZE(steps));
}

int main(void) {
	test_note_on();
	test_note_on2();
	test_note_on3();
	test_note_off();
	test_program_change();
	test_control_change();
	test_channel_pressure();
	test_pitch_bend();
	test_sysex();
	test_song_position_pointer();
	test_single_data_byte();

	puts("OK");
}
