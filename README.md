# midiparser: a minimal MIDI parsing library

Midiparser is a minimal C library that parses a MIDI byte stream into
MIDI messages.

This code originates from [gitlab.com/jacobvosmaer/crowbx](https://gitlab.com/jacobvosmaer/crowbx).

## Usage

Feed the MIDI byte stream to `midi_read` one byte at a time.

```
/* example.c  */
#include "midi.h"
#include <stdio.h>

int main(void) {
  int c;
  midi_parser parser = {0};

  puts("status data0 data1");
  while ((c = getchar()) != EOF) {
    midi_message msg = midi_read(&parser, c);
    if (msg.status)
      printf("  0x%02x  0x%02x  0x%02x\n", msg.status,
        msg.data[0], msg.data[1]);
  }
}
```

Example with two note-on messages. The second message in the stream
uses MIDI running status.

```
$ cc -o example midi.c example.c
$ printf '\x90\x11\x22\x33\x44' | ./example
status data0 data1
  0x90  0x11  0x22
  0x90  0x33  0x44
```

## System exclusive data

MIDI system exclusive data ("sysex") is a returned as a stream of
messages with status 0xf0. The payload is in the first data byte. The
first message of the stream can be recognized by the second data byte
which is set to 0xf0.

```
$ printf '\xf0\x01\x02\x03\x04\x05\xf7' | ./example
status data0 data1
  0xf0  0x01  0xf0
  0xf0  0x02  0x00
  0xf0  0x03  0x00
  0xf0  0x04  0x00
  0xf0  0x05  0x00
  0xf7  0x00  0x00
```

## Tests

The `test/` directory contains a test suite.

```
$ make -C test
cc    -c -o midi-test.o midi-test.c
cc  -c -o midi.o ../midi.c
cc   midi-test.o midi.o   -o midi-test
./midi-test
TEST: note on
TEST: note off
TEST: program change
TEST: control change
TEST: pitch bend
TEST: sysex
TEST: song position pointer
OK
```
